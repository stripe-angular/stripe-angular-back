<?php
include 'httpheaders.php';
include 'config.php';
require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// This is your real test secret API key.
\Stripe\Stripe::setApiKey($_ENV['STRIPE_SECRET_KEY']);
function calculateOrderAmount(array $items): int
{
    // Replace this constant with a calculation of the order's amount
    // Calculate the order total on the server to prevent
    // customers from directly manipulating the amount on the client
    return 1400;
}

try {
    // retrieve JSON from POST body
    $json_str = file_get_contents('php://input');
    $json_obj = json_decode($json_str);
    $paymentIntent = \Stripe\PaymentIntent::create([
        'amount' => calculateOrderAmount($json_obj->items),
        'currency' => 'eur',
        'description' => "My First Test Charge (created for API docs)",
        'receipt_email' => 'alex.rigueur@gmail.com',
        'payment_method_types' => ["card"],
        "metadata" => ["uid" => $json_obj->id]
    ]);
    $output = [
        'clientSecret' => $paymentIntent->client_secret,
    ];
    echo json_encode($output);
} catch (Error $e) {
    http_response_code(500);
    echo json_encode(['error' => $e->getMessage()]);
}
